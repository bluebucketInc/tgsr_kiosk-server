var path = require("path");
var express = require("express");
var http = require('http');
// static file compression middleware
// var compress = require("compression");
// middleware that allows you to parse request body, json, etc.
var bodyParser = require("body-parser");
// middleware to allow the general use of PUT and DELETE verbs
var methodOverride = require("method-override");
// logging middleware
var morgan = require("morgan");
// middleware to return X-Response-Time with a response
var responseTime = require("response-time");

// main app
var app = express();

app.use(morgan("dev"));
app.use(responseTime());

app.use(bodyParser());
app.use(methodOverride());

// app.use(compress());

// static files
app.use(express.static(path.join(__dirname, "public")));

const server = http.createServer(app);

// socket
var io = require("socket.io")(server);

//socket
io.on("connection", function(socket) {
	console.log("a user connected");

	// play pause event
	socket.on('video', function(state) {
		console.log('is video playing', state);
		io.emit('video', state);
	});
	
	// on mashup select
	socket.on('genre', function(genre) {
		console.log('currently playing', genre);
		io.emit('genre', genre);
	})

});

// port
server.listen(process.env.PORT || 5500);

console.log("server started on port: ", process.env.PORT || 5500);
